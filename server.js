const express = require("express");

// configurations
const connectDB = require("./src/config/mongo-db");

// Mongo DB connection
connectDB();

const app = express();
const port = process.env.PORT || 3001;

// Body parsing middleware (classic way)
app.use(express.json({ extended: false }));

// Routers Middlewares
app.use("/api/users", require("./src/routes/user-api"));
app.use("/api/auth", require("./src/routes/auth-api"));
app.use("/api/category", require("./src/routes/category-api"));

app.listen(port, () => console.log(`Server is listening in port ${port}`));
