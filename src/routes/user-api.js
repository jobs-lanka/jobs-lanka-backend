const express = require("express");

const router = express.Router();

// Controllers
const { createUser } = require("../controllers/user-controller");

// Validators
const { validateRegister } = require("../validators/user-validators");

// User Routers
router.post("/", validateRegister, createUser);

module.exports = router;
