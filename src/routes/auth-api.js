const express = require("express");

const auth = require("../middlewres/authorization");
const router = express.Router();

// Controllers
const {
  getUserAuthentication,
  loginUser,
} = require("../controllers/auth-controller");

// Validators
const { validateAuth } = require("../validators/auth-validators");

// Auth Routers
router.get("/", auth, getUserAuthentication);
router.post("/", validateAuth, loginUser);

module.exports = router;
