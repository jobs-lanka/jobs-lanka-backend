const express = require("express");

const auth = require("../middlewres/authorization");

const router = express.Router();

// Controllers
const {
  createCategory,
  findAllCategory,
  findOneCategory,
  updateCategory,
  deleteCategory,
} = require("../controllers/category-controller");

// Validators
const { validateCreateCategory } = require("../validators/category-validators");

// Category Routers
router.post("/", auth, validateCreateCategory, createCategory);
router.get("/", auth, findAllCategory);
router.get("/:id", auth, findOneCategory);
router.put("/:id", auth, updateCategory);
router.delete("/:id", auth, deleteCategory);

module.exports = router;
