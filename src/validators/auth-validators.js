const { check } = require("express-validator");

exports.validateAuth = [
  check("email", "Enter a valid email address").isEmail(),
  check("password", "Password is Required").exists(),
];
