const { check } = require("express-validator");

exports.validateCreateCategory = [
  check("category_name", "Category name is required").not().isEmpty(),
];
