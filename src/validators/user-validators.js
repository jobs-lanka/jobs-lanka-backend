const { check } = require("express-validator");

exports.validateRegister = [
  check("username", "Username is required").not().isEmpty(),
  check("email", "Enter a valid email address").isEmail(),
  check("password", "Password must be 6 chracters long").isLength({ min: 6 }),
  check("role", "Role is required").not().isEmpty(),
];
