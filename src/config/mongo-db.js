const mongoose = require("mongoose");

const credentials = require("./credentials");

const connectDB = async () => {
  try {
    await mongoose.connect(credentials.URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    });
    console.log(`Connected to Mongo DB`);
  } catch (error) {
    console.error(`Connection Failed : ${error}`);
    process.exit(1);
  }
};

module.exports = connectDB;
