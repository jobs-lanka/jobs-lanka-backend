const mongoose = require("mongoose");

const CategorySchema = mongoose.Schema({
  category_name: {
    type: String,
    required: true,
  },
  created_by: {
    type: mongoose.Schema.ObjectId,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
  updated_by: {
    type: mongoose.Schema.ObjectId,
  },
  updated_date: {
    type: Date,
    default: Date.now(),
  },
});

const Category = mongoose.model("Category", CategorySchema);

module.exports = Category;
