const jwt = require("jsonwebtoken");

const credentials = require("../config/credentials");

module.exports = function (req, res, next) {
  const token = req.header("x-auth-token");

  if (!token) {
    return res.status(401).json({ msg: "You have no authoriztion!!!" });
  }

  try {
    const decoded = jwt.verify(token, credentials.JWT_SECRET);
    req.user = decoded.user;
    next();
  } catch (error) {
    res.status(401).json({ msg: "Invalid Token!!!" });
  }
};
