const { validationResult } = require("express-validator");

const Category = require("../models/Category");
const User = require("../models/User");

exports.createCategory = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  try {
    const { category_name } = req.body;
    let category = await Category.findOne({ category_name });
    if (category) {
      return res
        .status(400)
        .json({ errors: [{ msg: "Category already exists" }] });
    }
    let user = await User.findById(req.user.id);

    if (user.role === "publisher") {
      category = new Category({
        category_name,
        created_by: req.user.id,
        updated_by: req.user.id,
      });
      category.save();
      return res.status(200).json(category);
    } else {
      return res
        .status(401)
        .json({ msg: "You should have publisher account to create category" });
    }
  } catch (error) {
    res.status(500).send(`Server Error`);
  }
};

exports.findAllCategory = async (req, res) => {
  try {
    const categories = await Category.find();
    return res.status(200).json(categories);
  } catch (error) {
    console.log(error);
    return res.status(500).send(`Server Error`);
  }
};

exports.findOneCategory = async (req, res) => {
  try {
    const { id } = req.params;
    const category = await Category.findOne({ _id: id });
    if (!category) {
      return res.status(400).json({ msg: "No category found" });
    }
    return res.status(200).json(category);
  } catch (error) {
    console.log(error);
    return res.status(500).send(`Server Error`);
  }
};

exports.updateCategory = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  try {
    const { id } = req.params;
    const { category_name } = req.body;
    const category = await Category.findOne({ _id: id });
    if (!category) {
      return res.status(400).json({ msg: "No category found" });
    }
    let user = await User.findById(req.user.id);

    if (user.role === "publisher") {
      category.category_name = category_name;
      category.updated_by = req.user.id;
      category.updated_date = Date.now();
      await category.save();
      return res.status(200).json(category);
    } else {
      return res
        .status(401)
        .json({ msg: "You should have publisher account to update category" });
    }
  } catch (error) {
    console.log(error);
    res.status(500).send(`Server Error`);
  }
};

exports.deleteCategory = async (req, res) => {
  try {
    const { id } = req.params;
    const category = await Category.findOne({ _id: id });

    if (!category) {
      return res.status(400).json({ msg: "No category found" });
    }

    let user = await User.findById(req.user.id);

    if (user.role === "publisher") {
      const deleted = await Category.deleteOne({ _id: id });
      if (deleted) {
        return res.status(200).json({ msg: "Deleted successfully" });
      }
    } else {
      return res
        .status(401)
        .json({ msg: "You should have publisher account to delete category" });
    }
  } catch (error) {
    console.log(error);
    return res.status(500).send(`Server Error`);
  }
};
